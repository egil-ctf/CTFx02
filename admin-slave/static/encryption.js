let alert_field = document.querySelector("#alert");
let form = document.querySelector("form");
let submit = document.getElementById("submit");
let itemIn = document.getElementById("item");
let combo = document.getElementById('combo');

submit.addEventListener("click", master);

function master(){

    let item = itemIn.value;

    item = encryption(item, key)

    alert_field.innerHTML = `${item.trim()}`

    combo.value = ''

    while(key.length > 0) {
      key.pop();
  }

    return false;
};


function encryption(msg, key) {
    value = msg.split("")
    dec = convert_dec(value)
    for (let i = 0; i < key.length; i++) {
      switch (key[i]) {
        case 1://+10
          dec = encrypt1(dec)
          break;
        case 2://-10
          dec = encrypt2(dec)
          break;
        case 3:// /2
          dec = encrypt3(dec)
          break;
        case 4:// * 2
          dec = encrypt4(dec)
          break;
        case 5:// << 2
          dec = encrypt5(dec)
          break;
        case 6:// >> 2
          dec = encrypt6(dec)
          break;
        default:
          break;
      }
    }
    return convert_ascii(dec).join("")
  }
  
  function encrypt1(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] + 10
    }
    return value
  }
  
  function encrypt2(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] - 10
    }
    return value
  }
  
  function encrypt3(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] / 2
    }
    return value
  }
  
  function encrypt4(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] * 2
    }
    return value
  }
  
  function encrypt5(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] << 2
    }
    return value
  }
  
  function encrypt6(value) {
  
    for (let i = 0; i < value.length; i++) {
      value[i] = value[i] >> 2
    }
    return value
  }
  
  function convert_dec(value) {
    dec = []
    for (let i = 0; i < value.length; i++) {
      dec[i] = value[i].charCodeAt(0)
    }
    return dec;
  }
  
  function convert_ascii(value) {
    ascii = []
    for (let i = 0; i < value.length; i++) {
      ascii[i] = String.fromCharCode(value[i])
    }
    return ascii;
  }