from functools import wraps
from json import JSONDecoder

from flask import request


def check_auth():
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):

            auth_username = None
            is_auth_valid = True

            # Retrieve auth cookie
            if "auth" in request.cookies:
                cookie = request.cookies["auth"]

                # Attempt to parse cookie
                try:
                    body = JSONDecoder().decode(cookie)
                except:
                    is_auth_valid = False
                    body = None

                # Extract username from cookie
                if body is not None:
                    if "token" not in body:
                        is_auth_valid = False
                    elif body['token'] != '972OEEelgcSVvQC3Tav4KNFx5v':
                        is_auth_valid = False 
                    else:
                        auth_username = body['token']

            return f(is_auth_valid=is_auth_valid, auth_username=auth_username, *args, **kwargs)
        return wrapper
    return decorator
