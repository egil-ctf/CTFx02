from flask import Flask, request, render_template, render_template_string, make_response, redirect
import logging
import json
import re

from typing import Optional
from api import check_auth

app = Flask(__name__)
app.logger.setLevel(logging.INFO)

# Numeric password (with restrictions)
def check_password(username, password) -> bool:
    return username == "sysadmin" and password == "23HawaiiModeratorJulDont"


@app.get('/')
@check_auth()
def get_easy_login(is_auth_valid: bool = True, auth_username: Optional[str] = None):

    if auth_username is None:
        response = make_response(redirect('/login'))

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response.delete_cookie("auth")


        return response


    return render_template('admin.html', username=auth_username),200

@app.route("/login", methods=['GET', 'POST'])
def get_login():
    if request.method == 'GET':
        return render_template('login.html'),200
    if not 'username' in request.form:
        app.logger.info("> Missing parameter 'username' in request")
        return render_template('login.html', result="bad"), 403
    username = request.form['username']

    if not 'password' in request.form:
        app.logger.info("> Missing parameter 'password' in request")
        return render_template('login.html', result="bad"), 403
    password = request.form['password']

    result = "success" if check_password(username, password) else "fail"
    status = 200 if result == "success" else 403

    if(status == 200):
        # Create response
        encoder = json.JSONEncoder()
        response = make_response(redirect("/"))
        response.set_cookie("auth", encoder.encode({'token': '972OEEelgcSVvQC3Tav4KNFx5v' }))

        return response


    return render_template("login.html", error_message="Authentication Failed"), status

