# Setup Networks

docker network create --gateway 172.16.1.1 --subnet 172.16.1.0/24 machines

# Run docker build

docker compose --build

# Inspect network

docker inspect network machines

Port that need to be exposed
80
200
