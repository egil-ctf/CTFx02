# CTFx02: Cryptic IT System Challenge

# Table of Contents
1. [Overview](#overview)
2. [Starting and Stopping the Project](#starting-and-stopping-the-project)
3. [Vulnerabilities](#vulnerabilities)
4. [Technology Used](#technology-used)

## Overview
CTFx02 is a Capture The Flag (CTF) challenge designed to simulate a small IT infrastructure, presenting a series of interconnected tasks. This challenge features a simulated IT manager and aims to teach students about reconnaissance in a controlled environment. The CTF encompasses a variety of tasks including brute-forcing, reverse engineering, cryptography, privilege escalation, and handling broken access controls.

## Starting and Stopping the Project

### Starting the Project
To start the project, use the following Docker Compose command. This will build and launch all the necessary containers in detached mode, allowing them to run in the background.

```docker compose up -d --build```

This command performs two main actions:

1. --build: Builds the images for the containers if they don't already exist or need updating.
2. -d: Runs the containers in detached mode, freeing up the terminal and running the containers in the background.

### Stopping the Project
When you're finished and want to stop all the running containers associated with the project, use the following command:

```docker compose down```

This command safely stops and removes all the containers, networks, and volumes created by docker compose up, cleaning up the system.

## Vulnerabilities

### Vulnerability #1: Brute-forcing the Login
- **Description**: The login page contains an email link to the head of IT. Contacting them yields a keylist, which can be used to brute-force the system password, granting initial access.

### Vulnerability #2: Cryptographic Keypad
- **Description**: The unlocked page features a specialized encryption application with a unique cryptography method. It performs arithmetic on ASCII values of input characters to conceal the SSH password of an exposed network machine. The challenge involves decrypting this password, either by reverse engineering the encryption method or brute-forcing the decryption code.

### Vulnerability #3: Reverse Engineering for Server Access
- **Description**: The accessible machine runs a custom program for server uptime checks, different from typical ping methods. By reverse engineering this program, participants can extract critical information about the master server and its credentials.

### Vulnerability #4: Privilege Escalation via Cron Job
- **Description**: On the master machine, there's a cron job updating a file at regular intervals. Understanding and exploiting this process allows for privilege escalation, providing access to increasingly secure data and uncovering a secret.

### Vulnerability #5: Accessing Secured User Data
- **Description**: Further exploration of the master machine post-escalation reveals two secured user accounts. Gaining access to these accounts is the final step in this challenge.

## Technology Used

- **Python**: Used for scripting and development of various components in the challenge.
- **HTML, CSS, and JavaScript**: Employed for creating the web interfaces and front-end interactions in the challenge.
- **Bash Scripting**: Utilized for automating tasks and simulating server-side operations.
- **Docker**: Provides an isolated and controlled environment for the CTF infrastructure.
- **C**: Used in the development of certain challenge components, particularly those involving reverse engineering and system-level interactions.

---

**Note**: CTFx02 is an educational challenge intended to provide practical experience in tackling real-world IT security issues. It should be conducted within ethical guidelines and in a secure environment.
